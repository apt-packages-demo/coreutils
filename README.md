# coreutils

Basic file, shell and text manipulation utilities. https://gnu.org/software/coreutils

### du
* du -sch $(ls -A) # to see hidden files and directories

#### Other tools with similar fonctionality
* https://tracker.debian.org/baobab 50000
* https://tracker.debian.org/ncdu 10000
* https://tracker.debian.org/filelight 5000
* https://tracker.debian.org/k4dirstat 1200
* https://tracker.debian.org/gdmap 500
* https://tracker.debian.org/xdiskusage 500
* https://tracker.debian.org/xdu 320
* https://tracker.debian.org/nnn 200
* https://tracker.debian.org/durep 180
* https://tracker.debian.org/qdirstat 160
* https://tracker.debian.org/agedu 150
* https://tracker.debian.org/gt5 150
* https://tracker.debian.org/vfu 100